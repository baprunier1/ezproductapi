package fr.klevh.lasttp.services;

import fr.klevh.lasttp.models.Cart;
import fr.klevh.lasttp.models.NutritionScore;
import fr.klevh.lasttp.models.Product;
import fr.klevh.lasttp.models.QualityRule;
import fr.klevh.lasttp.repositories.NutritionScoreRepository;
import fr.klevh.lasttp.repositories.QualityRuleRepository;
import fr.klevh.lasttp.utils.quality.QualityInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class QualityRuleServices {
    @Autowired
    private QualityRuleRepository qualityRuleRepository;
    @Autowired
    private NutritionScoreRepository nutritionScoreRepository;

    /**
     * Cast a long to its equivalent nutrition score
     * @param quality: long to be casted
     * @return the long equivalent nutrition score
     */
    public NutritionScore qualityFromLong(long quality){
        List<NutritionScore> nutritionScores = nutritionScoreRepository.findAll();

        for(NutritionScore ns : nutritionScores){
            if(ns.lowerBond <= quality && ns.upperBound >= quality){
                return ns;
            }
        }

        return null;
    }

    /**
     * Update the quality (long) and the quality information based on a positive rule
     * @param qualityInfo: quality information to be updated
     * @param quality: quality to be updated
     * @param qualityName: name of the positive quality
     * @param qualityRule: positive rule
     * @return the quality (long) updated
     */
    private long updatePositiveQuality(@NotNull QualityInfo qualityInfo, long quality, @NotNull String qualityName, @NotNull QualityRule qualityRule){
        quality -= qualityRule.points;

        if(qualityRule.points >= 2){
            qualityInfo.qualities.add(qualityName);
        }else if(qualityRule.points <= 0){
            qualityInfo.defects.add(qualityName);
        }

        qualityInfo.negativeQualities.put(qualityName, qualityRule.points);

        return quality;
    }

    /**
     * Update the quality (long) and the quality information based on a negative rule
     * @param qualityInfo: quality information to be updated
     * @param quality: quality to be updated
     * @param qualityName: name of the negative quality
     * @param qualityRule: positive rule
     * @return the quality (long) updated
     */
    private long updateNegativeQuality(@NotNull QualityInfo qualityInfo, long quality, @NotNull String qualityName, @NotNull QualityRule qualityRule) {
        quality += qualityRule.points;

        if(qualityRule.points <= 3){
            qualityInfo.qualities.add(qualityName);
        }else if(qualityRule.points >= 7){
            qualityInfo.defects.add(qualityName);
        }

        qualityInfo.positiveQualities.put(qualityName, qualityRule.points);

        return quality;
    }

    /**
     * Update quality (long) and quality information from a quality and its value
     * @param qualityInfo: quality information to be updated
     * @param quality: quality to be updated
     * @param qualityName: name of the quality
     * @param qualityValue: value of the quality
     * @return the quality (long) updated
     */
    private long updateQualityInfo(@NotNull QualityInfo qualityInfo, long quality, @NotNull String qualityName, @NotNull Double qualityValue){
        List<QualityRule> qualityRules = qualityRuleRepository.findAllByName(qualityName);
        qualityRules.sort(Comparator.comparing((QualityRule q) -> q.minBound));
        int i = 0;

        while(i < qualityRules.size() && qualityValue >= qualityRules.get(i).minBound){
            ++i;
        }
        if(i > 0) {
            --i;
        }

        QualityRule qualityRule = qualityRules.get(i);

        if(qualityRule.component.equals("P")){
            quality = updatePositiveQuality(qualityInfo, quality, qualityName, qualityRule);
        }else{
            quality = updateNegativeQuality(qualityInfo, quality, qualityName, qualityRule);
        }

        return quality;
    }

    /**
     * Get quality information from a product
     * @param product: product to get quality information from
     * @return quality information of the product
     */
    public QualityInfo getQualityInfo(@NotNull Product product){
        QualityInfo qualityInfo = new QualityInfo(product.code, product.name);
        Map<String, Double> qualities = product.getQualities();
        long quality = 0;

        for(Map.Entry<String, Double> qualityEntry : qualities.entrySet()){
            quality = updateQualityInfo(qualityInfo, quality, qualityEntry.getKey(), qualityEntry.getValue());
        }

        qualityInfo.qualityScore = qualityFromLong(quality);
        qualityInfo.quality = quality;

        qualityInfo.additives = new ArrayList<>(product.additives);

        return qualityInfo;
    }

    /**
     * Get nutrition score from a cart (average value of all its products nutrition score)
     * @param cart: cart to get information from
     * @return the nutrition score of the cart
     */
    public NutritionScore getQuality(@NotNull Cart cart){
        long quality = 0;

        for(Product product : cart.products){
            QualityInfo qualityInfo = getQualityInfo(product);
            quality += qualityInfo.quality;
        }

        if(cart.products.size() != 0) {
            return qualityFromLong(quality / cart.products.size());
        }else{
            return qualityFromLong(0);
        }
    }
}
