package fr.klevh.lasttp.services;

import fr.klevh.lasttp.models.Additive;
import fr.klevh.lasttp.models.Cart;
import fr.klevh.lasttp.models.NutritionScore;
import fr.klevh.lasttp.models.Product;
import fr.klevh.lasttp.repositories.AdditiveRepository;
import fr.klevh.lasttp.repositories.CartRepository;
import fr.klevh.lasttp.repositories.ProductRepository;
import fr.klevh.lasttp.utils.exceptions.BadRequestException;
import fr.klevh.lasttp.utils.quality.QualityInfo;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CartServices {
    @Autowired
    private ProductServices productServices;
    @Autowired
    private QualityRuleServices qualityRuleServices;

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private AdditiveRepository additiveRepository;

    /**
     * Create a new cart in DB
     * @param userMail: user mail to be bound to the cart
     * @return the cart created
     */
    public Cart createNewCart(@NotNull String userMail) {
        Cart cart = new Cart(userMail);

        cartRepository.save(cart);

        return cart;
    }

    /**
     * Load products bound to the cart and additives bound to those products
     * @param cart: cart from which to load products and additives
     */
    private void initCartFromDB(@NotNull Cart cart){
        Hibernate.initialize(cart.products);
        if (cart.products == null) {
            cart.products = new ArrayList<>();
        }else{
            for(Product p : cart.products){
                Hibernate.initialize(p.additives);
                if(p.additives == null){
                    p.additives = new ArrayList<>();
                }
            }
        }
    }

    /**
     * Find all carts with a certain user mail
     * @param userMail: user mail to use to find carts
     * @return all carts with a certain user mail
     */
    public List<Cart> findCartsByUserMail(@NotNull String userMail) {
        List<Cart> result = cartRepository.findByUserMail(userMail);

        if (result == null) {
            result = new ArrayList<>();
        }

        for (Cart cart : result) {
            initCartFromDB(cart);
        }

        return result;
    }

    /**
     * Find a cart by its id
     * @param id: id of the cart to be searched
     * @return the cart
     * @throws BadRequestException: cart not found
     */
    public Cart findCart(@NotNull String id) throws BadRequestException {
        Optional<Cart> optionalCart = cartRepository.findById(Long.valueOf(id));

        if (!optionalCart.isPresent()) {
            throw new BadRequestException("Cart not found");
        }

        Cart cart = optionalCart.get();
        initCartFromDB(cart);

        return cart;
    }

    /**
     * Add a product to a cart
     * @param cartID: id of the cart to add a product to
     * @param productID: bar code of the product to be added
     * @return The product added to the cart
     * @throws BadRequestException: cart or product not found
     */
    public Product addProductToCart(@NotNull String cartID, @NotNull String productID) throws BadRequestException {
        return addProductToCart(findCart(cartID), productID);
    }

    /**
     * Add a product to a cart
     * @param cart: cart to add a product to
     * @param productID: bar code of the product to be added
     * @return The product added to the cart
     * @throws BadRequestException: product not found
     */
    public Product addProductToCart(@NotNull Cart cart, @NotNull String productID) throws BadRequestException {
        Product product = productServices.findByID(productID);

        // saving product
        List<Additive> additives = product.additives;
        product.additives = null;
        product.cart = cart;
        productRepository.save(product);

        // saving additives
        for(Additive a : additives){
            a.productID = product.id;
            additiveRepository.save(a);
        }

        // updating product
        product.additives = additives;
        productRepository.save(product);

        // saving carts
        cart.products.add(product);
        cartRepository.save(cart);

        return product;
    }

    /**
     * Remove a product from a cart
     * @param cartID: id of the cart to remove a product from
     * @param productID: bar code of the product to be removed
     * @return true if the product was removed, false if not (if it was not bound to the cart)
     * @throws BadRequestException: no product in the cart or cart not found
     */
    public boolean removeProductFromCart(@NotNull String cartID, @NotNull String productID) throws BadRequestException {
        return removeProductFromCart(findCart(cartID), productID);
    }

    /**
     * Remove a product from a cart
     * @param cart: cart to remove a product from
     * @param productID: bar code of the product to be removed
     * @return true if the product was removed, false if not (if it was not bound to the cart)
     * @throws BadRequestException: no product in the cart
     */
    public boolean removeProductFromCart(@NotNull Cart cart, @NotNull String productID) throws BadRequestException {
        List<Product> products = productRepository.findAllByCodeAndCart(productID, cart);

        // getting product
        if (products == null || products.size() == 0) {
            throw new BadRequestException("No product in the cart");
        }
        // testing if cart have products
        if (cart.products == null || cart.products.size() == 0) {
            return false;
        }

        // trying to remove product
        Product product = products.get(0);
        boolean res = cart.products.remove(product);

        // if product removed
        if (res) {
            // deleting its additives
            for(Additive additive : product.additives){
                additiveRepository.delete(additive);
            }
            // deleting product
            productRepository.delete(product);
            // updating cart
            cartRepository.save(cart);
        }
        return res;
    }

    /**
     * Remove a cart from the database
     * @param id: id of the cart to remove
     * @return true
     * @throws BadRequestException: cart not found
     */
    public boolean removeCart(@NotNull String id) throws BadRequestException {
        Cart cart = findCart(id);

        for (Product p : cart.products) {
            for(Additive additive : p.additives){
                additiveRepository.delete(additive);
            }
            productRepository.delete(p);
        }
        cartRepository.delete(cart);

        return true;
    }

    /**
     * Get nutrition score of a cart
     * @param id: id of the cart to get nutrition score from
     * @return the nutrition score of the cart
     * @throws BadRequestException: cart not found
     */
    public NutritionScore getQuality(@NotNull String id) throws BadRequestException {
        return qualityRuleServices.getQuality(findCart(id));
    }

    /**
     * Get quality information about all products of the cart
     * @param id: id of the cart
     * @return quality information about all products of the cart
     * @throws BadRequestException: cart not found
     */
    public List<QualityInfo> getQualityInfo(@NotNull String id) throws BadRequestException {
        List<QualityInfo> qualityInfos = new ArrayList<>();

        for (Product p : findCart(id).products) {
            qualityInfos.add(productServices.getQualityInfoFromProduct(p));
        }

        return qualityInfos;
    }
}
