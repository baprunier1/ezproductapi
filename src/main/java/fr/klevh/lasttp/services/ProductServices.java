package fr.klevh.lasttp.services;

import fr.klevh.lasttp.models.Product;
import fr.klevh.lasttp.utils.exceptions.BadRequestException;
import fr.klevh.lasttp.utils.loaders.FoodLoader;
import fr.klevh.lasttp.utils.quality.QualityInfo;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Service
public class ProductServices {
    @Resource(name="offFoodLoader")
    private FoodLoader foodLoader;
    @Autowired
    private QualityRuleServices qualityRuleServices;

    /**
     * Find a product using its bar code (from foodLoader and not DB)
     * @param id: bar code of the product
     * @return the product
     * @throws BadRequestException: product not found
     */
    public Product findByID(@NotNull String id) throws BadRequestException{
        Product p = foodLoader.getFoodData(id);

        if(p == null){
            throw new BadRequestException("Product not found");
        }

        Hibernate.initialize(p.additives);
        if(p.additives == null){
            p.additives = new ArrayList<>();
        }

        return p;
    }

    /**
     * Get quality information from a product
     * @param id: bar code of the product
     * @return quality information from a product
     * @throws BadRequestException: product not found
     */
    public QualityInfo getQualityInfoFromProduct(@NotNull String id) throws BadRequestException{
        return getQualityInfoFromProduct(findByID(id));
    }

    /**
     * Get quality information from a product
     * @param product: product to get information from
     * @return quality information from a product
     */
    public QualityInfo getQualityInfoFromProduct(@NotNull Product product){
        return qualityRuleServices.getQualityInfo(product);
    }
}
