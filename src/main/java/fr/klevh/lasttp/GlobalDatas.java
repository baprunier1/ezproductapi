package fr.klevh.lasttp;

public class GlobalDatas {
    private GlobalDatas(){}

    // general
    public static final String baseUrl = "/EzProduct";

    // products
    public class Product{
        private Product(){}

        public static final String baseUrl = "/products";
        public static final String getUrl = "/product";
        public static final String getQualityUrl = "/quality";
    }

    // cart
    public class Cart{
        private Cart(){}

        public static final String baseUrl = "/carts";
        public static final String getUrl = "/cart";
        public static final String getAllUrl = "/carts";
        public static final String newUrl = "/new";
        public static final String addProductUrl = "/add";
        public static final String getQualityUrl = "/quality";
        public static final String getQualityScoreUrl = "/score";
        public static final String removeProductUrl = "/product/remove";
        public static final String removeUrl = "/remove";
    }
}
