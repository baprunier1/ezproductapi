package fr.klevh.lasttp.models;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Proxy;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Proxy(lazy = false)
@Entity
@Table(name = "carts")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    @ApiModelProperty(notes = "Cart id")
    public Long id;

    @Column(name = "usermail", nullable = false)
    @ApiModelProperty(notes = "Mail address of the owner of the cart")
    public String userMail;

    @OneToMany
    @ApiModelProperty(notes = "Product(s) the cart contains")
    public List<Product> products;

    public Cart(){
        this("<unknown>");
    }
    public Cart(String userMail) {
        this.userMail = userMail;
        products = new ArrayList<>();
    }

    public boolean equals(Object c) {
        return c.getClass().getName().equals(Cart.class.getName()) && ((Cart) c).id.equals(id) && ((Cart) c).userMail.equals(userMail);
    }
}
