package fr.klevh.lasttp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "additives")
public class Additive {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    @JsonIgnore
    public Long id;

    @Column(name = "productID", nullable = false, updatable = false)
    @JsonIgnore
    public Long productID;

    @Column(name = "name", nullable = false, updatable = false)
    public String name;

    @Column(name = "danger", nullable = false)
    public String danger;

    public Additive(){}

    public Additive(@NotNull Additive additive){
        id = null;
        productID = null;
        name = additive.name;
        danger = additive.danger;
    }
}
