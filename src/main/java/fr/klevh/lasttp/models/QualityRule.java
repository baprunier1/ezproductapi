package fr.klevh.lasttp.models;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.Column;

@Entity
@Table(name = "rule")
public class QualityRule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    public Long id;

    @Column(name = "name", nullable = false, updatable = false)
    public String name;

    @Column(name = "points", nullable = false)
    public Long points;

    @Column(name = "min_bound", nullable = false)
    public Double minBound;

    @Column(name = "component", nullable = false, updatable = false)
    public String component;
}
