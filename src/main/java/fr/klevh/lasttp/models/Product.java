package fr.klevh.lasttp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.klevh.lasttp.utils.quality.QualityName;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    @JsonIgnore
    public Long id;

    @ManyToOne
    @JsonIgnore
    public Cart cart;

    @Column(name = "code", nullable = false)
    @JsonIgnore
    public String code;

    @Column(name = "name", nullable = false)
    @ApiModelProperty(notes = "Product name")
    public String name;

    @Column(name = "energy")
    @ApiModelProperty(notes = "Product energy for 100g in kJ")
    public Double energy;

    @Column(name = "fat")
    @ApiModelProperty(notes = "Product saturated fat for 100g")
    public Double fat;

    @Column(name = "sugar")
    @ApiModelProperty(notes = "Product sugar for 100g")
    public Double sugar;

    @Column(name = "salt")
    @ApiModelProperty(notes = "Product salt for 100g")
    public Double salt;

    @Column(name = "fiber")
    @ApiModelProperty(notes = "Product fiber for 100g")
    public Double fiber;

    @Column(name = "protein")
    @ApiModelProperty(notes = "Product protein for 100g")
    public Double protein;

    @OneToMany
    public List<Additive> additives;

    public Product(){
        this("<unknown>", "<unknown>");
    }
    public Product(String code, String name){
        this.code = code;
        this.name = name;
        energy = 0.;
        fat = 0.;
        sugar = 0.;
        salt = 0.;
        fiber = 0.;
        protein = 0.;
        additives = new ArrayList<>();
    }

    public boolean equals(Object p){
        return p.getClass().getName().equals(Product.class.getName()) && ((Product)p).code.equals(code);
    }

    public Map<String, Double> getQualities(){
        Map<String, Double> result = new HashMap<>();

        result.put(QualityName.ENERGY, energy);
        result.put(QualityName.FAT, fat);
        result.put(QualityName.SUGAR, sugar);
        result.put(QualityName.SALT, salt);
        result.put(QualityName.FIBER, fiber);
        result.put(QualityName.PROTEIN, protein);

        return result;
    }
}
