package fr.klevh.lasttp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "nutrition_score")
public class NutritionScore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    @JsonIgnore
    public Long id;

    @Column(name = "classe", nullable = false, updatable = false)
    public String classe;

    @Column(name = "lower_bound", nullable = false, updatable = false)
    @JsonIgnore
    public Long lowerBond;

    @Column(name = "upper_bound", nullable = false, updatable = false)
    @JsonIgnore
    public Long upperBound;

    @Column(name = "color", nullable = false, updatable = false)
    public String color;
}
