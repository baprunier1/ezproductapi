package fr.klevh.lasttp.utils.responses;

public class BooleanResponse {
    public boolean bool;

    public BooleanResponse(boolean b){
        bool = b;
    }
}
