package fr.klevh.lasttp.utils.quality;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.klevh.lasttp.models.Additive;
import fr.klevh.lasttp.models.NutritionScore;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QualityInfo {
    @JsonIgnore
    private String code;
    @JsonIgnore
    public Long quality;
    @ApiModelProperty(notes = "Name of the product")
    private String name;

    @ApiModelProperty(notes = "Quality score of the product")
    public NutritionScore qualityScore;

    @ApiModelProperty(notes = "List of qualities of the product")
    public List<String> qualities;

    @ApiModelProperty(notes = "List of defects of the product")
    public List<String> defects;

    @ApiModelProperty(notes = "List of additives")
    public List<Additive> additives;

    @ApiModelProperty(notes = "Score of the product for each positive qualities (the higher the better)")
    public Map<String, Long> positiveQualities;

    @ApiModelProperty(notes = "Score of the product for each negative qualities (the lower the better)")
    public Map<String, Long> negativeQualities;

    public QualityInfo(String code, String name){
        this.code = code;
        this.name = name;
        negativeQualities = new HashMap<>();
        positiveQualities = new HashMap<>();
        defects = new ArrayList<>();
        qualities = new ArrayList<>();
    }

    public String getCode(){
        return code;
    }

    public String getName(){
        return name;
    }
}
