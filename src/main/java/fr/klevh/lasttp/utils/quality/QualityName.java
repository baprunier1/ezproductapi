package fr.klevh.lasttp.utils.quality;

public class QualityName {
    private QualityName(){}

    public static final String SALT = "salt_100g";
    public static final String SUGAR = "sugars_100g";
    public static final String FAT = "saturated-fat_100g";
    public static final String ENERGY = "energy_100g";
    public static final String PROTEIN = "proteins_100g";
    public static final String FIBER = "fiber_100g";
}
