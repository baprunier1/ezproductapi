package fr.klevh.lasttp.utils.loaders;

import fr.klevh.lasttp.models.Additive;

public interface AdditiveLoader {
    /**
     * Get an additive from its name
     * @param name: name of the additive
     * @return the additive
     */
    Additive getAdditive(String name);
}
