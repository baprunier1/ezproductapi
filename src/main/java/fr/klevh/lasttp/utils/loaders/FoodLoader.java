package fr.klevh.lasttp.utils.loaders;

import fr.klevh.lasttp.models.Product;
import org.springframework.stereotype.Component;

@Component
public interface FoodLoader {
    /**
     * Get a product from its bar code
     * @param code: bar code of the product
     * @return the product
     */
    Product getFoodData(String code);
}
