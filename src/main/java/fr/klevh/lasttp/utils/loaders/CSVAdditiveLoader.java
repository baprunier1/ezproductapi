package fr.klevh.lasttp.utils.loaders;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import fr.klevh.lasttp.models.Additive;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CSVAdditiveLoader implements AdditiveLoader{
    private Map<String, Additive> additives;
    private String path;

    /**
     * Construct a CSVAdditiveLoader from the path of the csv file. Read the file if the flag is true
     * @param path: path of the csv file
     * @param readNow: flag to tell whether or not the file should be read upon construction (true) or on first use (false)
     */
    public CSVAdditiveLoader(String path, boolean readNow){
        this.path = path;
        if(readNow){
            loadAdditives();
        }else {
            additives = null;
        }
    }

    /**
     * Construct a CSVAdditiveLoader from the path of the csv file. It will be read on its first use
     * @param path: path of the csv file
     */
    public CSVAdditiveLoader(String path){
        this(path, false);
    }

    /**
     * Read the file to load all additives from it
     */
    private void loadAdditives(){
        List<List<String>> records = new ArrayList<>();
        CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withIgnoreQuotations(true)
                .build();

        try {
            CSVReader csvReader = new CSVReaderBuilder(new FileReader((path)))
                    .withSkipLines(0)
                    .withCSVParser(parser)
                    .build();
            String[] values;
            while ((values = csvReader.readNext()) != null) {
                records.add(Arrays.asList(values));
            }
        }catch(IOException e){}

        additives = new HashMap<>();

        for(List<String> line : records){
            Additive additive = new Additive();
            additive.danger = line.get(line.size() - 1);
            additive.name = line.get(0).toUpperCase();
            additives.put(additive.name, additive);
        }
    }

    /**
     * Get an additive from its name
     * @param name: name of the additive
     * @return the additive
     */
    public Additive getAdditive(String name){
        if(additives == null){
            loadAdditives();
        }
        Additive additive = additives.get(name);
        if(additive == null){
            return null;
        }
        return new Additive(additive);
    }
}
