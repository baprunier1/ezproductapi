package fr.klevh.lasttp.utils.loaders;

import fr.klevh.lasttp.models.Additive;
import fr.klevh.lasttp.models.Product;
import org.springframework.boot.json.BasicJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Component
public class OFFFoodLoader implements FoodLoader {
    @Resource(name="csvAdditiveLoader")
    private AdditiveLoader additiveLoader;

    /**
     * Load nutritive values of a product
     * @param product: product to load nutritive values into
     * @param nutriments: map of nutritive values
     */
    private void loadProductNutritiveValues(@NotNull Product product, @NotNull Map<String, Object> nutriments){
        String[] names = {"fiber_100g", "salt_100g", "saturated-fat_100g", "energy_100g", "sugars_100g", "proteins_100g"};
        Double[] values = new Double[names.length];

        for(int i = 0; i < names.length; ++i){
            Object nutriment = nutriments.get(names[i]);
            try{
                values[i] = (Double) nutriment;
            }catch (ClassCastException e){
                values[i] = Double.parseDouble((String) nutriment);
            }
        }

        product.fiber   = values[0];
        product.salt    = values[1];
        product.fat     = values[2];
        product.energy  = values[3];
        product.sugar   = values[4];
        product.protein = values[5];
    }

    /**
     * Load all additives of a product from their names
     * @param product: product to load additives into
     * @param additives: names of the additives
     */
    private void loadProductAdditives(@NotNull Product product, @NotNull List<String> additives){
        for(String additiveName : additives){
            if(additiveName.length() > 3 && additiveName.matches("[a-z]{0,3}:.*")){
                additiveName = additiveName.substring(3);
            }
            Additive additive = additiveLoader.getAdditive(additiveName.toUpperCase());
            if(additive != null){
                product.additives.add(additive);
            }
        }
    }

    /**
     * Get a product from its bar code
     * @param code: bar code of the product
     * @return the product
     */
    public Product getFoodData(@NotNull String code) {
        final String uri = "http://fr.openfoodfacts.org/api/v0/produit/" + code + ".json";

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);

        JsonParser parser = new BasicJsonParser();

        Map<String, Object> productData = (Map<String, Object>) parser.parseMap(result).get("product");
        Map<String, Object> nutriments = (Map<String, Object>) productData.get("nutriments");

        Product product = new Product(code, (String)productData.get("product_name"));

        // loads nutriment values
        loadProductNutritiveValues(product, nutriments);

        // loads additive
        List<String> additives = (List<String>)productData.get("additives_tags");
        loadProductAdditives(product, additives);

        return product;
    }


}
