package fr.klevh.lasttp.repositories;

import fr.klevh.lasttp.models.NutritionScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NutritionScoreRepository extends JpaRepository<NutritionScore, Long> {
}
