package fr.klevh.lasttp.repositories;

import fr.klevh.lasttp.models.Cart;
import fr.klevh.lasttp.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Transactional
    List<Product> findAllByCodeAndCart(String code, Cart cart);
}
