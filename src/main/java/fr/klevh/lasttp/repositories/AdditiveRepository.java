package fr.klevh.lasttp.repositories;

import fr.klevh.lasttp.models.Additive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdditiveRepository extends JpaRepository<Additive, Long> {
    List<Additive> findAllByProductID(Long id);
}
