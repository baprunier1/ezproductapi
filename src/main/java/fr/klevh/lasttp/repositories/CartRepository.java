package fr.klevh.lasttp.repositories;

import fr.klevh.lasttp.models.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
    @Transactional
    List<Cart> findByUserMail(String userMail);
}
