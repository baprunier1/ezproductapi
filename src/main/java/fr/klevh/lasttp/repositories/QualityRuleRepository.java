package fr.klevh.lasttp.repositories;

import fr.klevh.lasttp.models.QualityRule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface QualityRuleRepository extends JpaRepository<QualityRule, Long> {
    @Transactional
    List<QualityRule> findAllByName(String name);
}
