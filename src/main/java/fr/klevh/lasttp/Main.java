package fr.klevh.lasttp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"fr.klevh.lasttp"})
public class Main {
    static public void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
