package fr.klevh.lasttp.controllers;

import fr.klevh.lasttp.utils.responses.StringResponse;
import fr.klevh.lasttp.utils.exceptions.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class GenericController {
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    StringResponse handleBadRequestException(BadRequestException e){
        return new StringResponse(e.getMessage());
    }
}
