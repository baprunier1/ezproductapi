package fr.klevh.lasttp.controllers;

import fr.klevh.lasttp.GlobalDatas;
import fr.klevh.lasttp.models.Product;
import fr.klevh.lasttp.services.ProductServices;
import fr.klevh.lasttp.utils.exceptions.BadRequestException;
import fr.klevh.lasttp.utils.quality.QualityInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(GlobalDatas.Product.baseUrl)
@Api(value = "producthandler", description = "Get information about a product such as its composition and its qualities and defects.")
public class ProductController extends GenericController{
    @Autowired
    private ProductServices productServices;

    @GetMapping(GlobalDatas.Product.getUrl)
    @ApiOperation(value = "Get information about a product", response = Product.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successfully retrieved product"),
            @ApiResponse(code = 400, message = "The product could not be found")
    })
    public Product getProduct(@RequestParam String id) throws BadRequestException {
        return productServices.findByID(id);
    }

    @GetMapping(GlobalDatas.Product.getQualityUrl)
    @ApiOperation(value = "Get quality information about a product", response = QualityInfo.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successfully retrieved product quality information"),
            @ApiResponse(code = 400, message = "The product could not be found")
    })
    public QualityInfo getQuality(@RequestParam String id) throws BadRequestException{
        return productServices.getQualityInfoFromProduct(id);
    }
}
