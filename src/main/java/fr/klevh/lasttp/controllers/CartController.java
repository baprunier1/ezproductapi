package fr.klevh.lasttp.controllers;

import fr.klevh.lasttp.GlobalDatas;
import fr.klevh.lasttp.models.Cart;
import fr.klevh.lasttp.models.NutritionScore;
import fr.klevh.lasttp.models.Product;
import fr.klevh.lasttp.services.CartServices;
import fr.klevh.lasttp.utils.responses.BooleanResponse;
import fr.klevh.lasttp.utils.exceptions.BadRequestException;
import fr.klevh.lasttp.utils.quality.QualityInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(GlobalDatas.Cart.baseUrl)
@Api(value = "carthandler", description = "Manage your carts and get information about their qualities.")
public class CartController extends GenericController{
    @Autowired
    private CartServices cartServices;



    // get requests

    @GetMapping(GlobalDatas.Cart.getUrl)
    @ApiOperation(value = "Get information about a cart", response = Cart.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successfully retrieved cart"),
            @ApiResponse(code = 400, message = "The cart could not be found")
    })
    public Cart getById(@RequestParam String id) throws BadRequestException {
        return cartServices.findCart(id);
    }

    @GetMapping(GlobalDatas.Cart.getAllUrl)
    @ApiOperation(value = "Get all carts attached to a mail address", response = List.class)
    @ApiResponse(code = 200, message = "A list of cart (which can be empty)")
    public List<Cart> getByUserMail(@RequestParam String userMail){
        return cartServices.findCartsByUserMail(userMail);
    }

    @GetMapping(GlobalDatas.Cart.getQualityScoreUrl)
    @ApiOperation(value = "Get quality score of a cart", response = List.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "The quality score of the cart"),
            @ApiResponse(code = 400, message = "The cart could not be found")
    })
    public NutritionScore getQualityScore(@RequestParam String id) throws BadRequestException{
        return cartServices.getQuality(id);
    }

    @GetMapping(GlobalDatas.Cart.getQualityUrl)
    @ApiOperation(value = "Get quality information of a cart", response = List.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of quality information of each products of the cart"),
            @ApiResponse(code = 400, message = "The cart could not be found")
    })
    public List<QualityInfo> getQuality(@RequestParam String id) throws BadRequestException{
        return cartServices.getQualityInfo(id);
    }



    // post requests

    @PostMapping(GlobalDatas.Cart.newUrl)
    @ApiOperation(value = "Create a new cart", response = Cart.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "The created cart")
    })
    public Cart newCart(@RequestParam String userMail){
        return cartServices.createNewCart(userMail);
    }

    @PostMapping(GlobalDatas.Cart.addProductUrl)
    @ApiOperation(value = "Add a product to the cart", response = Product.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "The product added to the cart"),
            @ApiResponse(code = 400, message = "The cart or the product could not be found")
    })
    public Product addProduct(@RequestParam String cartID, @RequestParam String productID) throws BadRequestException{
        return cartServices.addProductToCart(cartID, productID);
    }



    // delete requests

    @DeleteMapping(GlobalDatas.Cart.removeProductUrl)
    @ApiOperation(value = "Remove a product from a cart", response = Product.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "True if the product could be removed from the cart, false if not"),
            @ApiResponse(code = 400, message = "The cart or the product could not be found")
    })
    public BooleanResponse removeProduct(@RequestParam String cartID, @RequestParam String productID) throws BadRequestException{
        return new BooleanResponse(cartServices.removeProductFromCart(cartID, productID));
    }

    @DeleteMapping(GlobalDatas.Cart.removeUrl)
    @ApiOperation(value = "Delete a cart", response = Product.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "True if the cart could be deleted"),
            @ApiResponse(code = 400, message = "The cart could not be found")
    })
    public BooleanResponse removeCart(@RequestParam String id) throws BadRequestException{
        return new BooleanResponse(cartServices.removeCart(id));
    }
}
