package fr.klevh.lasttp.services;

import fr.klevh.lasttp.models.Cart;
import fr.klevh.lasttp.models.Product;
import fr.klevh.lasttp.utils.exceptions.BadRequestException;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CartServicesTest {

    @Autowired
    private CartServices cartServices;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String userMailOK = "oui@gmail.com";
    private String userMailKO = "non@gmail.com";

    private void assertCart(Cart cart, Long id, String userMail, int nbProducts) {
        assertEquals(id, cart.id);
        assertEquals(userMail, cart.userMail);
        assertEquals(nbProducts, cart.products.size());
    }

    @Test
    public void test1_createNewCart() {
        Cart createdCart = cartServices.createNewCart(userMailOK);

        assertCart(createdCart, 1L, userMailOK, 0);
    }

    @Test
    public void test2_findCart() throws BadRequestException {
        thrown.expect(BadRequestException.class);
        cartServices.findCart("2");

        Cart foundCart = cartServices.findCart("1");

        assertCart(foundCart, 1L, userMailOK, 0);
    }

    @Test
    public void test3_findCartByUserMail() {
        Cart createdCart2 = cartServices.createNewCart(userMailOK);

        assertCart(createdCart2, 2L, userMailOK, 0);

        List<Cart> cartList = cartServices.findCartsByUserMail(userMailKO);

        assertTrue(cartList.isEmpty());

        cartList = cartServices.findCartsByUserMail(userMailOK);

        assertEquals(2, cartList.size());
        for (int i = 0; i < 2; ++i) {
            assertCart(cartList.get(i), i + 1L, userMailOK, 0);
        }
    }

    @Test
    public void test4_removeCart() throws BadRequestException {
        boolean isRemoved = cartServices.removeCart("2");

        assertTrue(isRemoved);

        thrown.expect(BadRequestException.class);
        cartServices.findCart("2");
    }

    @Test
    public void test5_addProductToCart() throws BadRequestException {
        Cart foundCart = cartServices.findCart("1");

        assertCart(foundCart, 1L, userMailOK, 0);

        Product product1 = cartServices.addProductToCart(foundCart, "productId1");
        Product product2 = cartServices.addProductToCart(String.valueOf(foundCart.id), "productId2");

        foundCart = cartServices.findCart(String.valueOf(foundCart.id));

        assertCart(foundCart, 1L, userMailOK, 2);
        assertEquals(product1, foundCart.products.get(0));
        assertEquals(product2, foundCart.products.get(1));
    }

    @Test
    public void test6_removeProductFromCart() throws Exception {
        Cart foundCart = cartServices.findCart("1");

        assertCart(foundCart, 1L, userMailOK, 2);

        for (Product product : foundCart.products) {
            assertTrue(cartServices.removeProductFromCart(String.valueOf(foundCart.id), product.code));
        }

        foundCart = cartServices.findCart(String.valueOf(foundCart.id));

        assertCart(foundCart, 1L, userMailOK, 0);
    }
}
