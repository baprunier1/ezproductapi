package fr.klevh.lasttp.services;

import fr.klevh.lasttp.models.Additive;
import fr.klevh.lasttp.models.Product;
import fr.klevh.lasttp.utils.quality.QualityInfo;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductServicesTest {

    @Autowired
    private ProductServices productServices;

    @Test
    public void test2_findCart() {
        String productTestCode = "3387390320084";
        String productTestName = "CRUNCH Céréales";

        Product productTest = new Product(productTestCode, productTestName);

        productTest.energy = 1721.;
        productTest.fat = 3.2;
        productTest.sugar = 25.;
        productTest.salt = 0.0508;
        productTest.fiber = 5.6;
        productTest.protein = 7.2;

        Additive additive = new Additive();
        additive.name = "E322";
        additive.danger = "Ne pas abuser";

        productTest.additives.add(additive);

        QualityInfo qualityInfo = productServices.getQualityInfoFromProduct(productTest);

        assertEquals(productTestCode, qualityInfo.getCode());
        assertEquals(productTestName, qualityInfo.getName());

        assertEquals(Long.valueOf(4), qualityInfo.quality);
        assertEquals("Mangeable", qualityInfo.qualityScore.classe);
        assertEquals("yellow", qualityInfo.qualityScore.color);

        assertEquals(productTest.additives, qualityInfo.additives);
    }
}
