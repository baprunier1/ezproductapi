package fr.klevh.lasttp.controllers;

import fr.klevh.lasttp.GlobalDatas;
import fr.klevh.lasttp.models.Product;
import fr.klevh.lasttp.services.ProductServices;
import fr.klevh.lasttp.utils.exceptions.BadRequestException;
import fr.klevh.lasttp.utils.quality.QualityInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.BasicJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductServices services;

    private JsonParser jsonParser;

    public ProductControllerTest() {
        jsonParser = new BasicJsonParser();
    }

    @Test
    public void getProduct() throws Exception {
        String productIdOK = "oui";
        String productIdKO = "non";

        Mockito.when(services.findByID(productIdOK)).thenReturn(new Product("code", "product"));
        Mockito.when(services.findByID(productIdKO)).thenThrow(new BadRequestException(""));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Product.baseUrl + GlobalDatas.Product.getUrl + "?id=" + productIdKO)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Product.baseUrl + GlobalDatas.Product.getUrl + "?id=" + productIdOK)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        Map<String, Object> responseBody = jsonParser.parseMap(mvcResult.getResponse().getContentAsString());

        assertTrue(responseBody.containsKey("name"));
        assertEquals("product", responseBody.get("name"));
    }

    @Test
    public void getQualityScore() throws Exception {
        String productIdOK = "oui";
        String productIdKO = "non";

        Mockito.when(services.getQualityInfoFromProduct(productIdOK)).thenReturn(new QualityInfo("code", "quality"));
        Mockito.when(services.getQualityInfoFromProduct(productIdKO)).thenThrow(new BadRequestException(""));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Product.baseUrl + GlobalDatas.Product.getQualityUrl + "?id=" + productIdKO)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Product.baseUrl + GlobalDatas.Product.getQualityUrl + "?id=" + productIdOK)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        Map<String, Object> responseBody = jsonParser.parseMap(mvcResult.getResponse().getContentAsString());

        assertTrue(responseBody.containsKey("name"));
        assertEquals("quality", responseBody.get("name"));
    }
}
