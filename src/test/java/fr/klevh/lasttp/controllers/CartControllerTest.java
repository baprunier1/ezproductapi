package fr.klevh.lasttp.controllers;

import fr.klevh.lasttp.GlobalDatas;
import fr.klevh.lasttp.models.Cart;
import fr.klevh.lasttp.models.NutritionScore;
import fr.klevh.lasttp.models.Product;
import fr.klevh.lasttp.services.CartServices;
import fr.klevh.lasttp.utils.exceptions.BadRequestException;
import fr.klevh.lasttp.utils.quality.QualityInfo;
import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.BasicJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RunWith(SpringRunner.class)
@WebMvcTest(CartController.class)
public class CartControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CartServices services;

    private JsonParser jsonParser;

    public CartControllerTest() {
        jsonParser = new BasicJsonParser();
    }

    @Test
    public void newCart() throws Exception {
        String userMail = "loulou@gmail.com";

        Mockito.when(services.createNewCart(userMail)).thenReturn(new Cart(userMail));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.newUrl + "?userMail=" + userMail)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        Map<String, Object> responseBody = jsonParser.parseMap(mvcResult.getResponse().getContentAsString());

        assertTrue(responseBody.containsKey("userMail"));
        assertEquals(userMail, responseBody.get("userMail"));
    }

    @Test
    public void getById() throws Exception {
        String userMail = "oui@gmail.com";

        Mockito.when(services.findCart("1")).thenReturn(new Cart(userMail));
        Mockito.when(services.findCart("3")).thenThrow(new BadRequestException(""));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.getUrl + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        Map<String, Object> responseBody = jsonParser.parseMap(mvcResult.getResponse().getContentAsString());

        assertTrue(responseBody.containsKey("userMail"));
        assertEquals(userMail, responseBody.get("userMail"));

        mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.getUrl + "?id=3")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());
    }

    @Test
    public void getByUserMail() throws Exception {
        String userMailOK = "oui@gmail.com";
        String userMailKO = "non@gmail.com";

        List<Cart> cartList = new ArrayList<>();
        cartList.add(new Cart(userMailOK));
        cartList.add(new Cart(userMailOK));

        Mockito.when(services.findCartsByUserMail(userMailOK)).thenReturn(cartList);
        Mockito.when(services.findCartsByUserMail(userMailKO)).thenReturn(new ArrayList<>());

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.getAllUrl + "?userMail=" + userMailKO)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONArray jsonArray = new JSONArray(mvcResult.getResponse().getContentAsString());

        assertEquals(0, jsonArray.length());

        mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.getAllUrl + "?userMail=" + userMailOK)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        jsonArray = new JSONArray(mvcResult.getResponse().getContentAsString());

        assertEquals(2, jsonArray.length());
        assertTrue(jsonArray.getJSONObject(0).has("userMail"));
        assertEquals(userMailOK, jsonArray.getJSONObject(0).get("userMail"));
        assertTrue(jsonArray.getJSONObject(1).has("userMail"));
        assertEquals(userMailOK, jsonArray.getJSONObject(1).get("userMail"));
    }

    @Test
    public void removeCart() throws Exception {
        Mockito.when(services.removeCart("1")).thenReturn(true);
        Mockito.when(services.removeCart("2")).thenThrow(new BadRequestException(""));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.removeUrl + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.delete(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.removeUrl + "?id=2")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());
    }

    @Test
    public void addProduct() throws Exception {
        String productIdOK = "oui";
        String productIdKO = "non";

        Mockito.when(services.addProductToCart("3", productIdKO)).thenThrow(new BadRequestException(""));
        Mockito.when(services.addProductToCart("3", productIdOK)).thenReturn(new Product(productIdOK, "ok product"));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.addProductUrl + "?cartID=3&productID=" + productIdOK)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        Map<String, Object> responseBody = jsonParser.parseMap(mvcResult.getResponse().getContentAsString());

        assertTrue(responseBody.containsKey("name"));
        assertEquals("ok product", responseBody.get("name"));

        mvcResult = mvc.perform(MockMvcRequestBuilders.post(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.addProductUrl + "?cartID=3&productID=" + productIdKO)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());
    }

    @Test
    public void removeProduct() throws Exception {
        String productIdOK = "oui";
        String productIdKO = "non";

        Mockito.when(services.removeProductFromCart("1", productIdOK)).thenReturn(true);
        Mockito.when(services.removeProductFromCart("1", productIdKO)).thenThrow(new BadRequestException(""));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.removeProductUrl + "?cartID=1&productID=" + productIdKO)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.delete(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.removeProductUrl + "?cartID=1&productID=" + productIdOK)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());
    }

    @Test
    public void getQualityScore() throws Exception {
        NutritionScore nutritionScore = new NutritionScore();
        nutritionScore.classe = "diabete";

        Mockito.when(services.getQuality("1")).thenReturn(nutritionScore);
        Mockito.when(services.getQuality("2")).thenThrow(new BadRequestException(""));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.getQualityScoreUrl + "?id=2")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.getQualityScoreUrl + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        Map<String, Object> responseBody = jsonParser.parseMap(mvcResult.getResponse().getContentAsString());

        assertTrue(responseBody.containsKey("classe"));
        assertEquals("diabete", responseBody.get("classe"));
    }

    @Test
    public void getQuality() throws Exception {
        String okId = "oui";
        String koId = "non";

        List<QualityInfo> qualityInfos = new ArrayList<>();
        qualityInfos.add(new QualityInfo("1", "info1"));
        qualityInfos.add(new QualityInfo("2", "info2"));

        Mockito.when(services.getQualityInfo(okId)).thenReturn(qualityInfos);
        Mockito.when(services.getQualityInfo(koId)).thenThrow(new BadRequestException(""));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.getQualityUrl + "?id=" + koId)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.get(GlobalDatas.Cart.baseUrl + GlobalDatas.Cart.getQualityUrl + "?id=" + okId)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONArray jsonArray = new JSONArray(mvcResult.getResponse().getContentAsString());

        assertEquals(2, jsonArray.length());
        assertTrue(jsonArray.getJSONObject(0).has("name"));
        assertEquals("info1", jsonArray.getJSONObject(0).get("name"));
        assertTrue(jsonArray.getJSONObject(1).has("name"));
        assertEquals("info2", jsonArray.getJSONObject(1).get("name"));
    }
}
